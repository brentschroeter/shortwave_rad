var MAP_CONTRACTION = 2;
var MAX_INTENSITY = 239;
var MIN_INTENSITY = 0;
var M_PER_DEG_LAT = 110574;
var M_PER_DEG_LON_AT_EQUATOR = 111319;

var dataMin = d3.min(data, function(d) { return d3.min(d); });
var dataMax = d3.max(data, function(d) { return d3.max(d); });

var color = d3.scaleLinear()
  .domain([dataMin, dataMax])
  .range(['#000933', '#FFFFFF']);

function renderMap(contraction) {
  var start = Date.now();

  var svg = d3.select('#rad_map');
  svg.selectAll('*').remove();

  var intensityScale = d3.scaleLinear()
    .domain([dataMin, dataMax])
    .range([MAX_INTENSITY, MIN_INTENSITY]);

  for (var i = 0; i < data.length; i += contraction * MAP_CONTRACTION) {
    for (var j = 0; j < data[0].length; j += contraction * MAP_CONTRACTION) {
      var sample = 0;
      for (var k = 0; k < contraction * MAP_CONTRACTION; k++) {
        for (var l = 0; l < contraction * MAP_CONTRACTION; l++) {
          sample += data[i + k][j + l];
        }
      }
      sample /= Math.pow(contraction * MAP_CONTRACTION, 2);
      var scaled = parseInt(intensityScale(sample));
      svg.append('rect')
        .attr('x', j / MAP_CONTRACTION)
        .attr('y', i / MAP_CONTRACTION)
        .attr('width', contraction)
        .attr('height', contraction)
        .attr('fill', color(sample));
    }
  }

  var end = Date.now();

  d3.select('#map_render_time').text((end - start) + ' ms');
}

function renderHistogram(buckets) {
  var bucketSize = (dataMax - dataMin) / buckets;
  var hist = [];
  
  for (var i = 0; i < buckets; i++) {
    hist.push(0.0);
  }
  
  for (var i = 1; i < data.length - 1; i++) { // exclude North and South poles
    for (var j = 0; j < data[i].length; j++) {
      // increment bucket by the number of square meters reprented by the data point
      hist[parseInt(data[i][j] / bucketSize)] += (0.25 * M_PER_DEG_LAT) * (M_PER_DEG_LON_AT_EQUATOR * Math.cos(Math.PI / 180 * 0.25 * (i - 360)));
    }
  }

  var svg = d3.select('#histogram');
  svg.selectAll('*').remove();

  var y = d3.scaleLinear()
    .domain([0, d3.max(hist)])
    .range([0, 360]);

  var x = d3.scaleLinear()
    .domain([0, buckets])
    .range([20, 700]);

  var x2 = d3.scaleLinear()
    .domain([dataMin, dataMax])
    .range([20, 700]);

  for (var i = 0; i < buckets; i++) {
    svg.append('rect')
      .attr('x', x(i) + 720 / buckets / 4)
      .attr('y', 360 - y(hist[i]))
      .attr('width', 720 / buckets / 2)
      .attr('height', y(hist[i]))
      .attr('fill', color(i * bucketSize + dataMin));
  }

  var xAxis = d3.axisBottom(x2);
  svg.append('g')
    .attr('class', 'x axis')
    .attr('transform', `translate(0,${365})`)
    .call(xAxis);
}

d3.select('#map_16200_pts')
  .on('click', function() {
    d3.selectAll('.selection')
      .classed('selected', false);
    d3.select('#' + this.id)
      .classed('selected', true);
    renderMap(4);
  });
    
d3.select('#map_64800_pts')
  .on('click', function() {
    d3.selectAll('.selection')
      .classed('selected', false);
    d3.select('#' + this.id)
      .classed('selected', true);
    renderMap(2);
  });
    
d3.select('#map_259200_pts')
  .on('click', function() {
    d3.selectAll('.selection')
      .classed('selected', false);
    d3.select('#' + this.id)
      .classed('selected', true);
    renderMap(1);
  });
    
renderMap(4);
renderHistogram(20);

